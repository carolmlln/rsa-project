package test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;  
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RSA 
{
   private final static BigInteger one      = new BigInteger("1");
   private final static SecureRandom random = new SecureRandom();

   private BigInteger privateKey;
   private BigInteger publicKey;
   private BigInteger modulus;

   private static Scanner kb = new Scanner(System.in);
   
   public static void main(String[] args) 
   {
       System.out.println(Colours.RED + "Files Available: " + Colours.RESET);
       System.out.println(Colours.BLUE + "1. paragraphs" + Colours.RESET);
       System.out.println(Colours.BLUE + "2. characters" + Colours.RESET);
       System.out.println(Colours.BLUE + "3. words" + Colours.RESET);
       System.out.println(Colours.BLUE + "4. sentence" + Colours.RESET);
       System.out.println(Colours.BLUE + "5. numbers" + Colours.RESET);
        
        System.out.print(Colours.RED + "\n\nPlease enter the file name you wish to read from: " + Colours.RESET);
        
        //create a variable to hold the name of the file
        String filename = kb.nextLine();
        //create a list to hold the data from the file
        List<String> list = new ArrayList();
        
        String line = "";
        
        try
        {
            //enter the name of the file to be read
            Scanner myFile = new Scanner(new File(filename + ".txt"));
            int lineCount = 0;
            // while the file has content
            while(myFile.hasNextLine())
            {
                //add each line of the file to a variable line
                line += myFile.nextLine();
            }
            
            
            // for the length of line      
            for (int i = 0; i < line.length(); i += 124) 
            {
                //break the line into sub strings and add to the list
                list.add(line.substring(i, Math.min(i + 124, line.length())));
                //display the text to be encrypted
                System.out.println(Colours.GREEN + list.get(lineCount) + Colours.RESET);
                System.out.println(lineCount);
                lineCount++;
            }
            
            //declare N - the size of the world 
            //N is used in both the pulic and private keys
            int N = (1024);
            RSA key = new RSA(N);
            
            
            // this will print out the mod, public and private keys
            System.out.println(key);

            //create a byte array to hold the values of the list in an array of bytes
            byte[][] bytes = new byte[list.size()][];
            int c = 0;
            for (String s : list)
            {
                bytes[c] = s.getBytes();
                c++;
            }

            //create a BigInteger array for encrytpion
            BigInteger[] encryptMessage = new BigInteger[bytes.length];
            //the number of bytes to be converted
            System.out.println("\nlength of bytes:" + bytes.length);
            System.out.println(Colours.CYAN + "\nEncrypted Message: " + Colours.RESET);
            for (int i = 0; i < bytes.length; i++)
            {
                //add the bytes to the bigInteger array and encrypt
                encryptMessage[i] = key.encrypt(new BigInteger(bytes[i]));
                //display the encrypted data
                System.out.println( encryptMessage[i] );

            }
            
            //create a BigInteger array for decryption
            BigInteger[] decryptMessage = new BigInteger[encryptMessage.length];
            
            for (int i = 0; i < encryptMessage.length; i++)
            {
                //add the bytes to the bigInteger array and decrypt
                decryptMessage[i] = key.decrypt(encryptMessage[i]);
                //System.out.println(decryptMessage[i] );

            }
            
            

            byte[][] data = new byte[decryptMessage.length][];

            for(int i = 0; i < decryptMessage.length; i++)
            {
                //send the decrypted BigInteger array values to a new array of byte arrays
                data[i] = decryptMessage[i].toByteArray();
            }
            
            //list to hold decrypted message
            List<String> decryptedMessage = new ArrayList();

            for (byte[] b : data) 
            {
                //convert the byte array back to a string
                decryptedMessage.add(new String(b));
            }
            
            //display the decrypted message
            System.out.println(Colours.GREEN + "\nDecrypted Message: " + Colours.RESET);
            System.out.println(decryptedMessage);
            
        }
        catch (FileNotFoundException fe) {
            System.out.println("That file was not found in the system, sorry.");
        }
        
        
   
     
   }
   
   
   
   // generate an N-bit (roughly) public and private key
   RSA(int N) 
   {
        //Choose a large random prime number p and declare another q
        BigInteger p = BigInteger.probablePrime(N, random);
          BigInteger q;

          do
          {
              //Choose a large random prime number q
              q = BigInteger.probablePrime(N, random);
          }
          while (q.compareTo(p) == 1); //check that p & q are not equal
          
          //as long as p & q are not equal calculate phi
          BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));
          
          //calculate the modulus
          modulus    = p.multiply(q);

          // common value in practice = 2^16 + 1
          publicKey  = new BigInteger("65537"); 
          //calculate the private key the mod inverse of phi
          privateKey = publicKey.modInverse(phi);
   }


   BigInteger encrypt(BigInteger message) 
   {
      return message.modPow(publicKey, modulus);
   }

   BigInteger decrypt(BigInteger encrypted) 
   {
      return encrypted.modPow(privateKey, modulus);
   }

   public String toString() 
   {
      String s = "";
      s += "\npublic  = " + publicKey  + "\n" ;
      s += "\nprivate = " + privateKey + "\n";
      s += "\nmodulus = " + modulus + "\n";
      return s;
   }
 

   
}